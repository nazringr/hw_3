package com.company;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";

        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";

        scedule[2][0] = "Tuesday";
        scedule[2][1] = "play tennis";

        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go swimming";

        scedule[4][0] = "Thursday";
        scedule[4][1] = "finish a book";

        scedule[5][0] = "Friday";
        scedule[5][1] = "do shopping";

        scedule[6][0] = "Saturday";
        scedule[6][1] = "hang out, dance ";


        while (true) {
            System.out.println("Please, input the day of the week:");
            Scanner scan = new Scanner(System.in);
            String weekday = scan.nextLine().toLowerCase();
            if (weekday.equals("exit")){ break;}

            switch (weekday) {
                case "sunday": {
                    System.out.println("Your tasks for " + scedule[0][0] + " :" + scedule[0][1]);
                    break;
                }
                case "monday": {
                    System.out.println("Your tasks for " + scedule[1][0] + " :" + scedule[1][1]);
                    break;
                }

                case "tuesday": {
                    System.out.println("Your tasks for " + scedule[2][0] + " :" + scedule[2][1]);
                    break;
                }
                case "wednesday": {
                    System.out.println("Your tasks for " + scedule[3][0] + " :" + scedule[3][1]);
                    break;
                }
                case "thursday": {
                    System.out.println("Your tasks for " + scedule[4][0] + " :" + scedule[4][1]);
                    break;
                }
                case "friday": {
                    System.out.println("Your tasks for " + scedule[5][0] + " :" + scedule[5][1]);
                    break;
                }
                case "saturday": {
                    System.out.println("Your tasks for " + scedule[6][0] + " :" + scedule[6][1]);
                    break;
                }
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }

        }

    }
}

